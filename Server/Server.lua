--[[
    DiscordToFiveM (Taco's Fork)
    This is a fork of DiscordToFiveM that adds additional paths to handle weather and time manipulation
]]

local DTFVersion = '1.1.1'

local IdentifiersUsed = {'license', 'steam'}

local Colors = {
	-- Reds
	lightsalmon = {255, 160, 122},
	salmon = {250, 128, 114},
	darksalmon = {233, 150, 122},
	lightcoral = {240, 128, 128},
	indianred = {205, 92, 92},
	crimson = {220, 20, 60},
	firebrick = {178, 34, 34},
	red = {255, 0, 0},
	darkred = {139, 0, 0},
	maroon = {128, 0, 0},
	tomato = {255, 99, 71},
	orangered = {255, 69, 0},
	palevioletred = {219, 112, 147},

	-- Blues
	aliceblue = {240, 248, 255},
	lavender = {230, 230, 250},
	powderblue = {176, 224, 230},
	lightblue = {173, 216, 230},
	lightskyblue = {135, 206, 250},
	skyblue = {135, 206, 235},
	deepskyblue = {0, 191, 255},
	lightsteelblue = {176, 196, 222},
	dodgerblue = {30, 144, 255},
	cornflowerblue = {100, 149, 237},
	steelblue = {70, 130, 180},
	cadetblue = {95, 158, 160},
	mediumslateblue = {123, 104, 238},
	slateblue = {106, 90, 205},
	darkslateblue = {72, 61, 139},
	royalblue = {65, 105, 225},
	blue = {0, 0, 255},
	mediumblue = {0, 0, 205},
	darkblue = {0, 0, 139},
	navy = {0, 0, 128},
	midnightblue = {25, 25, 112},
	blueviolet = {138, 43, 226},
	indigo = {75, 0, 130},

	-- Greens
	lawngreen = {124, 252, 0},
	chartreuse = {127, 255, 0},
	limegreen = {50, 205, 50},
	lime = {0, 255, 0},
	forestgreen = {34, 139, 34},
	green = {0, 128, 0},
	darkgreen = {0, 100, 0},
	greenyellow = {173, 255, 47},
	yellowgreen = {154, 205, 50},
	springgreen = {0, 255, 127},
	mediumspringgreen = {0, 250, 154},
	lightgreen = {144, 238, 144},
	palegreen = {152, 251, 152},
	darkseagreen = {143, 188, 143},
	mediumseagreen = {60, 179, 113},
	lightseagreen = {32, 178, 170},
	seagreen = {46, 139, 87},
	olive = {128, 128, 0},
	darkolivegreen = {85, 107, 47},
	olivedrab = {107, 142, 35},
	-- Cyans
	lightcyan = {224, 255, 255},
	cyan = {0, 255, 255},
	aqua = {0, 255, 255},
	aquamarine = {127, 255, 212},
	mediumaquamarine = {105, 205, 170},
	paleturquoise = {175, 238, 238},
	turquoise = {64, 224, 208},
	mediumturquoise = {72, 209, 204},
	darkturquoise = {0, 206, 209},
	lightseagreen = {32, 178, 170},
	darkcyan = {0, 139, 139},
	teal = {0, 128, 128}
}

local WeatherStrings = {
    clear = true,
    extrasunny = true,
    clouds = true,
    overcast = true,
    rain = true,
    clearing = true,
    thunder = true,
    smog = true,
    foggy = true,
    xmas = true
}

-- Registers a HTTP request and handles it
SetHttpHandler(function(req, res)
	local Value = 'Bad Request'
	local path = URLEncode(req.path):sub(2)
	if path:sub(1, GetConvar('DTF_Password'):len()) == GetConvar('DTF_Password') then
		path = path:sub(GetConvar('DTF_Password'):len() + 2)
		local Sender = path:sub(1, path:find('/') - 1)
		path = path:sub(Sender:len() + 2)

		if req.method == 'GET' then
			if path == 'chkcon' then
				Value = 'Connection successful'
            elseif path == 'getclients' then
				Value = 'Nothing'
				local Clients = ""

				for _, ID in ipairs(GetPlayers()) do
					if tonumber(ID) < 10 then
						ID = '0' .. tostring(ID)
					end
					Clients = Clients .. ID .. '   |   ' .. GetPlayerName(ID) .. ';'
				end

				if Clients:len() > 0 then
					Value = Clients
				end
			elseif path:sub(1, 12) == 'sendcolorrgb' then
                local AdditionalInfo = ''
                local parts = stringsplit(path:sub(14), '/')
                local Message = parts[4] or nil
                local R = tonumber(parts[1]) or nil
                local G = tonumber(parts[2]) or nil
                local B = tonumber(parts[3]) or nil
                if Message == nil then
                    Value = { Message = 'Failed to send message', Details = 'Message not entered', AdditionalInfo = '' }
                elseif R == nil then
                    Value = { Message = 'Failed to send message', Details = 'R in RGB not specified or not a number', AdditionalInfo = '' }
                elseif G == nil then
                    Value = { Message = 'Failed to send message', Details = 'G in RGB not specified or not a number', AdditionalInfo = '' }
                elseif B == nil then
                    Value = { Message = 'Failed to send message', Details = 'B in RGB not specified or not a number', AdditionalInfo = '' }
                else
                    if R > 255 then
                        R = 255
                    elseif R < 0 then
                        R = 0
                    end
                    if G > 255 then
                        G = 255
                    elseif G < 0 then
                        G = 0
                    end
                    if B > 255 then
                        B = 255
                    elseif B < 0 then
                        B = 0
                    end
                    TriggerClientEvent('chatMessage', -1, Sender, {R, G, B}, Message)
                    if UsingDiscordBot then
					    TriggerEvent('DiscordBot:ToDiscord', 'Chat', Sender, Message, '', true)
				    end
                    Value = { Message = 'Message sent successfully', Details = 'Message ' .. Message .. '\nSender ' .. Sender, AdditionalInfo = 'Color {' .. R .. ',' .. G .. ',' .. B .. '}' }
                end
			elseif path:sub(1, 4) == 'send' then
				local Message = path:sub(6)
				if UsingDiscordBot then
					TriggerEvent('DiscordBot:ToDiscord', 'Chat', Sender, Message, '', true)
				end
				TriggerClientEvent('chatMessage', -1, Sender, {222, 199, 132}, Message)
				Value = 'Sent'
				print(Sender .. '(BOT): ' .. Message)
			elseif path:sub(1, 4) == 'kick' then
				Value = nil
				path = path:sub(6)
				local parts = stringsplit(path, '/')
				local ServerID = tonumber(parts[1])
				local Reason = parts[2] or ''
				local Name = GetPlayerName(ServerID)
				if Name then
					DropPlayer(ServerID, 'Kicked! Reason: ' .. Reason)
					print('>> ' .. Lang.Kicked .. ' ' .. Name .. '\n>> ' .. Lang.Reason .. ': ' .. Reason)
					if SendKickToChat then
						TriggerClientEvent('chatMessage', -1, 'DiscordToFiveM', {222, 199, 132}, Lang.Kicked .. ' ' .. Name .. '\n' .. Lang.Reason .. ': ' .. Reason)
					end
					if UsingDiscordBot then
						TriggerEvent('DiscordBot:ToDiscord', 'Chat', 'DiscordToFiveM', Lang.Kicked .. ' ' .. Name .. '\n' .. Lang.Reason .. ': ' .. Reason, '', true)
					end
					Value = 'Kicked ' .. Name
				end
			elseif path:sub(1, 3) == 'ban' then
				Value = nil
				path = path:sub(5)
				local parts = stringsplit(path, '/')
				local ServerID = tonumber(parts[1])
				local Reason = parts[2] or ''
				local Name = GetPlayerName(ServerID):gsub(';', ',')
				if Name then
					local UTC = os.time(os.date('*t'))
					for i, IdentifierUsed in ipairs(IdentifiersUsed) do
						local ID = GetIDFromSource(IdentifierUsed, ServerID)
						if ID ~= nil then
							local Content = DTF_Load('BannedPlayer', IdentifierUsed:upper() .. '.txt')
							DTF_Save('BannedPlayer', IdentifierUsed:upper() .. '.txt', Content .. Name .. ';' .. ID .. ';' .. tostring(UTC) .. ';' .. Reason .. ';' .. BanDuration .. '\n')
						end
					end
					DropPlayer(ServerID, 'Banned! Reason: ' .. Reason)
					local Dur
					if BanDuration == 0 then
						Dur = Lang.Forever
					else
						Dur = BanDuration .. ' ' .. Lang.Hours
					end
					print('>> ' .. Lang.Banned .. ' ' .. Name .. '\n>> ' .. Lang.Reason .. ': ' .. Reason .. '\n>> ' ..  Lang.Duration .. ': ' .. Dur)
					if SendBanToChat then
						TriggerClientEvent('chatMessage', -1, 'DiscordToFiveM', {222, 199, 132}, Lang.Banned .. ' ' .. Name .. '\n' .. Lang.Reason .. ': ' .. Reason .. '\n' ..  Lang.Duration .. ': ' .. Dur)
					end
					if UsingDiscordBot then
						TriggerEvent('DiscordBot:ToDiscord', 'Chat', 'DiscordToFiveM', Lang.Banned .. ' ' .. Name .. '\n' .. Lang.Reason .. ': ' .. Reason .. '\n' ..  Lang.Duration .. ': ' .. Dur, '', true)
					end
					Value = 'Banned ' .. BanDuration .. ' ' .. Name
				end
            elseif path:sub(1, 7) == 'weather' then
                Value = nil
                path = path:sub(9)
                local ServerID = tonumber(path:sub(1, 2))
                local Weather = path
                Weather = string.lower(Weather)
                if Weather then
                    if WeatherStrings[Weather] then
                        TriggerClientEvent('DTF:UpdateWeather', -1, Weather)
                        Value = { Message = 'Set Weather Successfully', Details = Weather, AdditionalInfo = '' }
                    else
                        Value = { Message = 'Set Weather Failed', Details = 'Invalid Weather ' .. Weather, AdditionalInfo = 'Available options are CLEAR, EXTRASUNNY, CLOUDS, OVERCAST, RAIN, CLEARING, THUNDER, SMOG, FOGGY, XMAS (all case insensitive)' }
                    end
                else
                    Value = { Message = 'Set Weather Failed', Details = 'No Weather Entered', AdditionalInfo = 'Available options are CLEAR, EXTRASUNNY, CLOUDS, OVERCAST, RAIN, CLEARING, THUNDER, SMOG, FOGGY, XMAS (all case insensitive)' }
                end
            elseif path:sub(1, 4) == 'time' then
                Value = ''
                path = path:sub(6)
                local Parameters = {}
                local AdditionalInfo = ''
                for Match in path:gmatch('([^/]+)') do
                    table.insert(Parameters, Match)
                end
                if TableLength(Parameters) == 0 then
                    Value = { Message = 'Set Time Failed', Details = 'No time entered', AdditionalInfo = '' }
                elseif TableLength(Parameters) == 1 then
                    AdditionalInfo = AdditionalInfo .. 'Minutes and seconds not set, set to 0;'
                    table.insert(Parameters, 0)
                    table.insert(Parameters, 0)
                elseif TableLength(Parameters) == 2 then
                    table.insert(Parameters, 0)
                    AdditionalInfo = AdditionalInfo .. 'Seconds not set, set to 0;'
                end
                Hours = tonumber(Parameters[1])
                Minutes = tonumber(Parameters[2])
                Seconds = tonumber(Parameters[3])
                if not Hours then
                    Value = { Message = 'Set Time Failed', Details = 'Hours is not a number', AdditionalInfo = AdditionalInfo }
                elseif not Minutes then
                    Value = { Message = 'Set Time Failed', Details = 'Minutes is not a number', AdditionalInfo = AdditionalInfo }
                elseif not Seconds then
                    Value = { Message = 'Set Time Failed', Details = 'Seconds is not a number', AdditionalInfo = AdditionalInfo }
                else
                    if Hours > 23 then
                        AdditionalInfo = AdditionalInfo .. 'Hours was over 23, set to 23;'
                        Hours = 23
                    elseif Hours < 0 then
                        AdditionalInfo = AdditionalInfo .. 'Hours was less than 0, set to 0;'
                        Hours = 0
                    end
                    if Minutes > 59 then
                        AdditionalInfo = AdditionalInfo .. 'Minutes was over 59, set to 59;'
                        Minutes = 59
                    elseif Minutes < 0 then
                        AdditionalInfo = AdditionalInfo .. 'Minutes was less than 0, set to 0;'
                        Minutes = 0
                    end
                    if Seconds > 59 then
                        AdditionalInfo = AdditionalInfo .. 'Seconds was over 59, set to 59;'
                        Seconds = 59
                    elseif Seconds < 0 then
                        AdditionalInfo = AdditionalInfo .. 'Seconds was less than 0, set to 0;'
                        Seconds = 0
                    end
                    TriggerClientEvent('DTF:UpdateTime', -1, Hours, Minutes, Seconds)
                    Value = { Message = 'Set Time Successfully', Details = 'Set time to ' .. Hours .. ':' .. Minutes .. ':' .. Seconds, AdditionalInfo = AdditionalInfo }
                end
			end
		end
	end
	res.send(json.encode(Value))
end)

AddEventHandler('playerConnecting', function(playerName, setKickReason) --Checks if a Player is banned and kicks him if needed
	for i, IdentifierUsed in ipairs(IdentifiersUsed) do
		local UTC = os.time(os.date('*t'))
		local Content = DTF_Load('BannedPlayer', IdentifierUsed:upper() .. '.txt')
		if Content ~= nil and Content ~= '' then
			local Splitted = stringsplit(Content, '\n')
			if #Splitted >= 1 then
				for i, line in ipairs(Splitted) do
					local lineSplitted = stringsplit(line, ';')
					local BanName = lineSplitted[1]
					local BanID = lineSplitted[2]
					local BanTimeThen = tonumber(lineSplitted[3])
					local BanReason = lineSplitted[4]
					local BanDuration = tonumber(lineSplitted[5])
					if BanID == GetIDFromSource(IdentifierUsed, source) then
						if BanDuration == 0 then
							setKickReason('You are banned forever! Reason: ' .. BanReason)
							CancelEvent()
							return
						else
							local Duration = BanDuration * 3600
							local PassedTime = UTC - BanTimeThen
							if PassedTime > Duration then
								DTF_Save('BannedPlayer', IdentifierUsed:upper() .. '.txt', Content:gsub(line .. '\n', ''))
							else
								local Remaining
								if math.floor(Duration - PassedTime) < 60 then
									Remaining = math.floor(Duration - PassedTime) .. ' Seconds'
								elseif round((math.floor(Duration - PassedTime) / 60), 1) < 60 then
									Remaining = round((math.floor(Duration - PassedTime) / 60), 1) .. ' Minutes'
								else
									Remaining = round((round((math.floor(Duration - PassedTime) / 60), 1) / 60), 1) .. ' Hours'
								end
								setKickReason('You are still banned for ' .. Remaining .. '! Reason: ' .. BanReason)
								CancelEvent()
								return
							end
						end
					end
				end
			end
		end
	end
end)

-- Functions
function URLEncode(String)
	String = string.gsub(String, "+", " ")
	String = string.gsub(String, "%%(%x%x)", function(H)
		return string.char(tonumber(H, 16))
	end)
	return String
end

function stringsplit(input, seperator, count)
	count = count or 9999
	if seperator == nil then
		seperator = '%s'
	end
	
	local t={} ; i=1
	
	for str in string.gmatch(input, '([^'..seperator..']+)') do
		t[i] = str
		i = i + 1
		if count < 0 then
			break
		end
		count = count - 1
	end
	
	return t
end

function round(num, numDecimalPlaces)
	local mult = 10^(numDecimalPlaces or 0)
	return math.floor(num * mult + 0.5) / mult
end

function GetOSSep()
	if os.getenv('HOME') then
		return '/'
	end
	return '\\'
end

function DTF_Save(Folder, File, Content)
	local UnusedBool = SaveResourceFile(GetCurrentResourceName(), Folder .. GetOSSep() .. File, Content, -1)
end

function DTF_Load(Folder, File)
	local Content = LoadResourceFile(GetCurrentResourceName(), Folder .. GetOSSep() .. File)
	return Content
end

function GetIDFromSource(Type, ID) --(Thanks To WolfKnight [forum.FiveM.net])
    local IDs = GetPlayerIdentifiers(ID)
    for k, CurrentID in pairs(IDs) do
        local ID = stringsplit(CurrentID, ':')
        if (ID[1]:lower() == string.lower(Type)) then
            return ID[2]:lower()
        end
    end
    return nil
end

function TableLength(Table)
    local Count = 0
    for _ in pairs(Table) do Count = Count + 1 end
    return Count
end

-- Removed version checking
--[[ Version Checking down here, better don't touch this
local CurrentVersion = '2.0.0'
local GithubResourceName = 'DiscordToFiveMBot'

PerformHttpRequest('https://raw.githubusercontent.com/Flatracer/FiveM_Resources/master/' .. GithubResourceName .. '/VERSION', function(Error, NewestVersion, Header)
	PerformHttpRequest('https://raw.githubusercontent.com/Flatracer/FiveM_Resources/master/' .. GithubResourceName .. '/CHANGES', function(Error, Changes, Header)
		print('\n')
		print('##############')
		print('## ' .. GetCurrentResourceName())
		print('##')
		print('## Current Version: ' .. CurrentVersion)
		print('## Newest Version: ' .. NewestVersion)
		print('##')
		if CurrentVersion ~= NewestVersion then
			print('## Outdated')
			print('## Check the Topic')
			print('## For the newest Version!')
			print('##############')
			print('CHANGES: ' .. Changes)
		else
			print('## Up to date!')
			print('##############')
		end
		print('\n')
	end)
end)]]
print('\n##############\n##\n## DiscordToFiveM (Taco\'s Fork)\n##\n## Version: ' .. DTFVersion .. '\n##\n##############')