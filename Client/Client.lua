RegisterNetEvent('DTF:UpdateTime')
AddEventHandler('DTF:UpdateTime', function(Hours, Minutes, Seconds)
    NetworkOverrideClockTime(Hours, Minutes, Seconds)
end)

RegisterNetEvent('DTF:UpdateWeather')
AddEventHandler('DTF:UpdateWeather', function(WeatherString)
    SetOverrideWeather(WeatherString)
end)