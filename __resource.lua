resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'Receives Discord messages and prints them out in-game, allows you to send messages to the server, kick and ban players, and set the weather and time'

client_script 'Client/Client.lua'

server_script {
	'Config.lua',
	'Language.lua',
	'Server/Server.lua',
}
